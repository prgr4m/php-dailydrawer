<?php

namespace App;

class DailyDrawer
{
    private $limit;
    private $sort;

    public function __construct(int $limit = 5)
    {
        $this->setLimit($limit);
        $this->sort = "new";
    }

    public function setLimit(int $limit): void
    {
        // just in case this code is run under an older version of php
        if (! is_int($limit)) {
            throw new \Exception('$limit is not an integer');
        }
        $this->limit = $limit;
    }

    public function buildURL(): string
    {
        $subreddit_url = "https://www.reddit.com/r/dailydraw/new.json";
        $subreddit_url .= "?sort={$this->sort}&limit={$this->limit}";
        return $subreddit_url;
    }

    public function fetchSubredditContents(): array
    {
        // curl automagically verifies...
        $context = stream_context_create(['ssl' => ['verify_peer' => true]]);
        $contents = file_get_contents($this->buildURL(), false, $context);
        // print_r(json_decode($contents, true));
        return json_decode($contents, true);
    }

    public function getDrawingThemes(bool $omitWeekly = true): array
    {
        // see: reddit.com/dev/api
        $retArray = [];
        $contentArray = $this->fetchSubredditContents();

        foreach ($contentArray['data']['children'] as $child) {
            if (array_key_exists('title', $child['data'])) {
                if ($omitWeekly && preg_match('/Weekly Suggestion Box/i', $child['data']['title'])) {
                    continue;
                }
                $retArray[] = $child['data']['title'];
            }
        }
        return $retArray;
    }
}
