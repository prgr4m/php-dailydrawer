<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use VCR\VCR;
use App\DailyDrawer;

class DailyDrawerTest extends TestCase
{
    public $dd;

    protected function setUp()
    {
        VCR::configure()
            ->setCassettePath('tests/fixtures')
            ->setStorage('json');
        VCR::turnOn();
        VCR::insertCassette('dailydraw');
        $this->dd = new DailyDrawer();
    }

    protected function tearDown()
    {
        VCR::eject();
        VCR::turnOff();
    }

    public function testBuildURL()
    {
        $this->dd->setLimit(10);
        $testUrl = "https://www.reddit.com/r/dailydraw/new.json?sort=new&limit=10";
        $this->assertEquals($testUrl, $this->dd->buildURL());
    }

    public function testFetchSubredditContents()
    {
        $this->assertTrue(is_array($this->dd->fetchSubredditContents()));
    }

    public function testGetDrawingThemes()
    {
        // expecting the following values from the cassette since the
        // default limit set is 5 most recent
        $expectedFull = [
            'Apr. 23: Samurai',
            'Apr. 22: Free draw!',
            'Apr. 21: (Texture) Muscles',
            'Apr. 18: (Perspective) Storm',
            'Weekly Suggestion Box (Apr 24 - Apr 30)'
        ];
        $this->assertEquals($expectedFull, $this->dd->getDrawingThemes(false));
        $expectedOmit = array_slice($expectedFull, 0, -1);
        $this->assertEquals($expectedOmit, $this->dd->getDrawingThemes());
    }
}
